package ictgradschool.industry.lab04.ex03;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.
        int goal = (int)(Math.random() * 100 + 1);
        int guess = 0;
        System.out.print("Enter your guess (1 – 100): ");
        guess = Integer.parseInt(Keyboard.readInput());


        while (guess != goal){

            if (guess < goal) {
                System.out.print("Too low, try again\nEnter your guess (1 – 100): ");
                guess = Integer.parseInt(Keyboard.readInput());
            }

            else {
                System.out.print("Too high, try again\nEnter your guess (1 – 100): ");
                guess = Integer.parseInt(Keyboard.readInput());
            }

        }


        System.out.println("Perfect!\nGoodbye");





    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
