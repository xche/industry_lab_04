package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;
    public static final int QUIT = 4;


    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        String name = getUsersName();
        int playerChoice = getUsersChoice();
        int computerChoice = getComputersChoice();
        displayPlayerChoice(name, playerChoice);
        if (playerChoice != QUIT){
            displayComputersChoice(computerChoice);
            userWins(playerChoice, computerChoice);
            String result = getResultString(playerChoice, computerChoice);
            if (userWins(playerChoice, computerChoice)){
                System.out.println(name + " wins because " + result);
            }
            else if (playerChoice == computerChoice){
                System.out.println("No one wins because" + result);
            }
            else System.out.println("The computer wins because " + result);
        }






    }

    private String getUsersName(){
        System.out.print("Hi! What is your name? ");
        String name = Keyboard.readInput();
        return name;
    }

    private int getUsersChoice(){
        System.out.println();
        System.out.print("1. Rock\n2. Scissors\n3. Paper\n4. Quit\n     Enter choice: ");

        int playerChoice = Integer.parseInt(Keyboard.readInput());
        return playerChoice;

    }

    private int getComputersChoice(){
        int computerChoice = (int)(Math.random() * 3 + 1);
        return computerChoice;
    }

    public void displayPlayerChoice(String name, int playerChoice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)


        System.out.println();
        if (playerChoice == ROCK) {
            System.out.println(name + " chose rock.");
        }
        else if (playerChoice == SCISSORS) {
            System.out.println(name + " chose scissors.");
        }
        else if (playerChoice == PAPER) {
            System.out.println(name + " chose paper.");
        }
        else System.out.println("Goodbye " + name + ". Thanks for playing :)");


    }

    private void displayComputersChoice(int computerChoice) {
        if (computerChoice == ROCK) {
            System.out.println("Computer chose rock.");
        }
        else if (computerChoice == SCISSORS) {
            System.out.println("Computer chose scissors.");
        }
        else System.out.println("Computer chose paper.");

    }


    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.
        if (playerChoice - computerChoice == -1 || playerChoice - computerChoice == 2){
            return true;
        }

        return false;
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        if (playerChoice == computerChoice){
            return TIE;
        }
        else if (userWins(playerChoice, computerChoice) && playerChoice == PAPER || !userWins(playerChoice, computerChoice) && computerChoice == PAPER){
            return PAPER_WINS;

        }
        else if (userWins(playerChoice, computerChoice) && playerChoice == ROCK || !userWins(playerChoice, computerChoice) && computerChoice == ROCK){
            return ROCK_WINS;

        }
        else return SCISSORS_WINS;




    }



    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
