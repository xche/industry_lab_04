package ictgradschool.industry.lab04.ex05;

/**
 * Created by xingjianche on 18/03/2017.
 */
public class Pattern {

    int num;
    char symbol;


    public Pattern(int num, char symbol) {

        this.num = num;
        this.symbol = symbol;


    }

    public int getNumberOfCharacters() {

        return this.num;

    }

    public void setNumberOfCharacters(int num) {

        this.num = num;

    }

    public String toString() {
        String symbol = "";
        for (int j = 0; j < num; j++){
            symbol += this.symbol;
        }
        return symbol;
    }



}
